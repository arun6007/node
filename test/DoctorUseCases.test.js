const expect = require('chai').expect
const should = require('chai').should()

const DoctorModel = require('../src/enterprise_business_rules/entities/Doctor')
const DoctorRepositoryMysql = require('../src/interface_adapters/storage/DoctorRepositoryMySql')
const DoctorRepository = require('../src/application_business_rules/repositories/DoctorRepository')

const mockDoctorRepository = new DoctorRepository(new DoctorRepositoryMysql())

const DoctorUseCases = require('../src/application_business_rules/use_cases/DoctorUseCases')

const doctorUseCases = new DoctorUseCases()


describe('Testing doctor use cases, ADD doctor METHOD', () => {
    it('#should return an object of doctor type if valid input are provided', async () => {
        const doctor = new DoctorModel(null, 'KiranKumar', '300')
        const result = await doctorUseCases.addDoctor(doctor, mockDoctorRepository)
        expect(result).to.be.an('object')
        result.should.have.property('doctor_name')
        result.should.have.property('doctor_specialization')
        result.should.have.property('id')
    })
    it('#should return an array with error if inputs are not valid', async () => {
        const doctor = new DoctorModel(null, null, null)
        const result = await doctorUseCases.addDoctor(doctor, mockDoctorRepository)
        expect(result).to.be.an('array')
        expect(result).length(2)
    })
})

describe('Testing Doctor Use Cases, GET Doctor METHOD', () => {
    it('#should return an array of doctors', async () => {
        const result = await doctorUseCases.getAllDoctors(mockDoctorRepository)
        expect(result).to.be.an('array')
    })
})



describe('Testing doctor Use Cases, UPDATE doctor METHOD', () => {
    it('#should return 1 in array if doctor is updated succesfully', async () => {
        const doctor = new DoctorModel(null, 'dharshini', '400')
        const seqdoctor = await doctorUseCases.adddoctor(doctor, mockdoctorRepository)
        const updatedoctor = new DoctorModel(seqdoctor.id, 'Sowjanya', '200')
        const result = await doctorUseCases.updatedoctor(seqdoctor.id, updatedoctor, mockdoctorRepository)
        expect(result).to.be.an('array')
        expect(result[0]).to.equal(1)
    })
    it('#should return 0 in array if doctor id is not valid', async () => {
        const updatedoctor = new DoctorModel(1000, 'Ranchi', '120')
        const result = await doctorUseCases.updatedoctor(1000, updatedoctor, mockdoctorRepository)
        expect(result).to.be.an('array')
        expect(result[0]).to.equal(0)
    })
    it('#should return with array if the input is not valid', async () => {
        const result = await doctorUseCases.updatedoctor(1, { doctor_name: null, doctor_specialization: null }, mockdoctorRepository)
        expect(result).to.be.an('array')
        expect(result).length(2)
        const result2 = await doctorUseCases.updatedoctor(null, { doctor_name: null, doctor_specialization: null }, mockdoctorRepository)
        expect(result2).to.be.an('array')
        expect(result2).length(3)
    })

})

describe('Testing DOCTOR use cases, DELETE DOCTOR METHOD', () => {
    it('#should delete the doctor if valid id is provided and return one', async () => {
        const doctor = new DoctorModel(null, 'alwar', '100')
        const newDoctor = await doctorUseCases.addDoctor(doctor, mockDoctorRepository)
        const result = await doctorUseCases.deletedoctor(newDoctor.id, mockDoctorRepository)
        expect(result).to.equal(1)
    })
    it('#should return error message if the id is null or undefinded', async () => {
        const result = await DoctorUseCases.deleteDoctor(null, mockDoctorRepository)
        result.should.be.a('string')  
    })
    it('#should return zero if invalid id is provided', async () => {
        const result = await doctorUseCases.deletedoctor(5000, mockDoctorRepository)
        expect(result).to.equal(0)
    })
})