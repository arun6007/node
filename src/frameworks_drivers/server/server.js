const express = require('express')
const app = express()
const morgan = require('morgan')
const serveIndex = require('serve-index')

// Middlewares
const tokenChecker = require('../../interface_adapters/middlewares/TokenChecker')
//const studentRoutes = require('../../interface_adapters/controllers/StudentController')
//const doctorRoutes = require('../../interface_adapters/controllers/DoctorController')
const user_profileRoutes = require('../../interface_adapters/controllers/User_profileController')
const userRoutes = require('../../interface_adapters/controllers/UserController')
const leaveRoutes = require('../../interface_adapters/controllers/LeaveController')
// Logger
app.use(morgan("dev"))
app.use('/upload', express.static('upload'), serveIndex('upload', {'icons': true}));
// Body Parser
app.use(express.urlencoded({ limit: '50mb', extended: false }))
app.use(express.json())

// handle cors Errors
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, POST, PATCH, DELETE, GET");
        return res.status(200).json({});
    }
    next();
});

// routes to be published here
// app.use('/api/v1/student',studentRoutes)

// app.use('/api/v1/doctor',doctorRoutes)

// app.use('/api/v1/alldoctor',doctorRoutes)
app.use('/api/v1/update',user_profileRoutes)
app.use('/api/v1/update',userRoutes)
app.use('/api/v1/leave',leaveRoutes)


// app.use('/api/v1/deletedoctor',doctorRoutes)
// app.use('/api/v1/updatedoctor',doctorRoutes)
// app.use('/api/v1/getsingledoctor',doctorRoutes)

// Error Handling
app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});


module.exports = app