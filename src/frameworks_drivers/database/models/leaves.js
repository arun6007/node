module.exports = (sequelize, type) => {
    return sequelize.define('leaves', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
         user_id: {
            type: type.INTEGER,
           // primaryKey: true,
            allowNull: true
           // autoIncrement: true
        },
         start_date: {
            type: type.DATE,
            primaryKey: true,
            allowNull: true
            //autoIncrement: true
        },
          end_date: {
            type: type.DATE,
            primaryKey: true,
            allowNull: true
           // autoIncrement: true
        },
         reason: {
            type: type.STRING,
            allowNull: true
        },
          status: {
            type: type.STRING,
            allowNull: true
        },
          approver_id: {
            type: type.INTEGER,
            allowNull: true
        },
        leave_type: {
            type: type.STRING,
            allowNull: true
        }
    },{
        freezTableName: true,
        updatedAt: false,
        createdAt: false
    })
}