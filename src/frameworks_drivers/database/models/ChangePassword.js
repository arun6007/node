module.exports = (sequelize, type) => {
    return sequelize.define('users', {
        id: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
         username: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false
           // autoIncrement: true
        },
         password: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false
            //autoIncrement: true
        },
          name: {
            type: type.STRING,
            primaryKey: true,
            allowNull: false
           // autoIncrement: true
        },

        role: {
            type: type.BIGINT,
            allowNull: false
        }
    },{
        freezTableName: true,
        updatedAt: false,
        createdAt: false
    })
}