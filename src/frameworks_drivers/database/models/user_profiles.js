module.exports = (sequelize, type) => {
    return sequelize.define('user_profiles', {
        id: {
            type: type.INTEGER,
            primaryKey: true,
            allowNull: false,
            autoIncrement: true
        },
         user_id: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false
           // autoIncrement: true
        },
         role_id: {
            type: type.BIGINT,
            primaryKey: true,
            allowNull: false
            //autoIncrement: true
        },
          updated_time: {
            type: type.DATE,
            primaryKey: true,
            allowNull: false
           // autoIncrement: true
        },
        profile: {
            type: type.JSON,
            allowNull: false
        },
         name: {
            type: type.STRING,
            allowNull: false
        },
        role_name: {
            type: type.STRING,
            allowNull: false
        }
    },{
        freezTableName: true,
        updatedAt: false,
        createdAt: false
    })
}