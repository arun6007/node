const Sequelize = require("sequelize");

//const studentModel = require("./models/Student");
//const doctorModel = require("./models/Doctor");
const user_profilesModel = require("./models/user_profiles");
const user = require("./models/ChangePassword");
const leaves = require("./models/leaves");

const sequelize = new Sequelize("stqcv3", "postgres", "evpl@123", {
  host: "65.0.78.143",
  port: 5432,
  dialect: "postgres",
  pool: {
    max: 10,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
});


//const Student = studentModel(sequelize, Sequelize);
//const Doctor = doctorModel(sequelize, Sequelize);
const User_profiles = user_profilesModel(sequelize, Sequelize);
const User = user(sequelize, Sequelize);
const Leaves = leaves(sequelize, Sequelize);

// Model RelationShip

module.exports = sequelize;



