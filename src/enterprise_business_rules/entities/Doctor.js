module.exports = class {
    constructor(id= null, doctor_name, doctor_specialization){
        this.id = id
        this.doctor_name = doctor_name
        this.doctor_specialization = doctor_specialization
    }
}