module.exports = class{
    constructor(repository){
        this.repository = repository
    }

    // add a new city
    add(leaveEntity){
        return this.repository.add(leaveEntity)
    }

    // update existing city
    update(id,userEntity){
        return this.repository.update(id, userEntity)
    }

    // delete existing city
    delete(id){
        return this.repository.delete(id)
    }

    // get all cities
    getAll(){
        return this.repository.getAll()
    }
    getById(user_id){
        return this.repository.getAllById(user_id)
    }
    get(id){
        return this.repository.get(id)
    }
    login(doctorEntity){
        return this.repository.check(doctorEntity)
    }
}