module.exports = class{
    constructor(repository){
        this.repository = repository
    }

    // add a new city
    add(user_profileEntity){
        return this.repository.add(user_profileEntity)
    }

    // update existing city
    update(id,userEntity){
        return this.repository.update(id, userEntity)
    }

    // delete existing city
    delete(id){
        return this.repository.delete(id)
    }

    // get all cities
    getAll(){
        return this.repository.getAll()
    }
    getById(id){
        return this.repository.getAll(id)
    }
    get(id){
        return this.repository.get(id)
    }
    login(doctorEntity){
        return this.repository.check(doctorEntity)
    }
}