module.exports = class {
    getAllDoctors (Repository)  {
        return Repository.getAll()
    }
    addDoctor (doctorEntity, Repository) { 
        return Repository.add(doctorEntity)
    }
    update (id, userEntity, Repository)  {
        return Repository.update(id,userEntity)
    }
    deleteDoctor (id, Repository)  {
        return Repository.delete(id)
    }
    getUserById (id, Repository)  {
        return Repository.getById(id)
    }
    getSingleDoctor (id, Repository)  {
        return Repository.get(id)
    }
    logincheck (doctorEntity, Repository)  {
        return Repository.login(doctorEntity)
    }

}