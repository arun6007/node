module.exports = class {
    getAllleaves (Repository)  {
        return Repository.getAll()
    }
    addLeave (leaveEntity, Repository) { 
        return Repository.add(leaveEntity)
    }
    update (id, userEntity, Repository)  {
        return Repository.update(id,userEntity)
    }
    deleteDoctor (id, Repository)  {
        return Repository.delete(id)
    }
    getLeavesById (user_id, Repository)  {
        return Repository.getById(user_id)
    }
    getSingleDoctor (id, Repository)  {
        return Repository.get(id)
    }
    logincheck (doctorEntity, Repository)  {
        return Repository.login(doctorEntity)
    }

}