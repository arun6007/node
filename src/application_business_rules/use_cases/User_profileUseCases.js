module.exports = class {
    getAllDoctors (Repository)  {
        return Repository.getAll()
    }
    addDoctor (doctorEntity, Repository) { 
        return Repository.add(doctorEntity)
    }
    update (id, user_profileEntity, Repository)  {
        return Repository.update(id,user_profileEntity)
    }
    deleteDoctor (id, Repository)  {
        return Repository.delete(id)
    }
    getDoctorById (id, Repository)  {
        return Repository.getById(id)
    }
    getSingleDoctor (id, Repository)  {
        return Repository.get(id)
    }
    logincheck (doctorEntity, Repository)  {
        return Repository.login(doctorEntity)
    }

}