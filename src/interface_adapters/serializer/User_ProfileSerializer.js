const _serializeSingleUser_profile = (userprofile) => {
    return {
      'id': userprofile.id,
      'user_id': userprofile.user_id,
        'role_id': userprofile.role_id,
         'role_name': userprofile.role_name,
         'name': userprofile.name,
         'profile': userprofile.profile
         //'updated_time': userprofile.updated_time
    };
  };
  
  module.exports = class {
  
    serialize(data) {
      if (!data) {
        throw new Error('Expect data to be not undefined nor null');
      }
      if (Array.isArray(data)) {
        return data.map(userprofile => _serializeSingleUser_profile(userprofile));
      }
      return _serializeSingleUser_profile(data);
    }
  
  };