const _serializeSingleLeave = (leave) => {
    return {
      'id': leave.id,
      'user_id': leave.user_id,
        'start_date': leave.start_date,
         'end_date': leave.end_date,
         'reason': leave.reason,
         'status': leave.status,
         'approver_id': leave.approver_id,
         'leave_type': leave.leave_type
         //'updated_time': userprofile.updated_time
    };
  };
  
  module.exports = class {
  
    serialize(data) {
      if (!data) {
        throw new Error('Expect data to be not undefined nor null');
      }
      if (Array.isArray(data)) {
        return data.map(leave => _serializeSingleLeave(leave));
      }
      return _serializeSingleLeave(data);
    }
  
  };