const _serializeSingleUser = (user) => {
    return {
      'id': user.id,
      'username': user.username,
        'password': user.password,
         'name': user.name,
         'role': user.role
         //'updated_time': userprofile.updated_time
    };
  };
  
  module.exports = class {
  
    serialize(data) {
      if (!data) {
        throw new Error('Expect data to be not undefined nor null');
      }
      if (Array.isArray(data)) {
        return data.map(user => _serializeSingleUser(user));
      }
      return _serializeSingleUser(data);
    }
  
  };