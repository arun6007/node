const router = require('express').Router()
const _ = require('lodash')
const multer = require('multer');
const path = require('path');
const jwt = require('jsonwebtoken')
const config = require('../../../config')
const message = require('../../../message')
const UserProfileSerializer = require('../serializer/User_ProfileSerializer')

const UserProfileUseCases = require('../../application_business_rules/use_cases/User_profileUseCases')
const UserProfileRepositoryMyql = require('../storage/User_ProfileRepositoryMySql')
const UserProfileRepository = require('../../application_business_rules/repositories/User_profileRepository')
const jwtKey = "It's a secret"
const jwtExpirySeconds = 300
var storage = multer.diskStorage({
  destination: (req, file, cb) => {
    // console.log("DocumentType:"+documenttypeid+":"+userinfoid)
   // console.log("Parameter to set destination:" + req.params.type);
     cb(null, "./upload");
    //TODO Based on Document Type we can set different paths below 'change ./uploads for different conditon
    // cb(null, "./uploads");
  },
  filename: (req, file, cb) => {
    //console.log(req)
    cb(
      null,
      file.fieldname + "-" + Date.now() + path.extname(file.originalname)
    );
  },
});


const upload = multer({
  storage,
  fileFilter: (req, file, cb) => {
    if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
      req.fileValidationError = "Only image files are allowed!!!";
      //return cb(new Error('Invalid file format'),false) ;
      return cb(new Error("Invalid File Format"), false);
    }
    cb(null, true);
  },
});
////main
// initializing city repository 
const userprofileRepository = new UserProfileRepository(new UserProfileRepositoryMyql())
const userprofileUseCases = new UserProfileUseCases()
//////signup
router.post('/', async (req, res) => {
    // inputs
    const { doctor_name, doctor_specialization } = req.body

    // treatment
    const result = await doctorUseCases.addDoctor({ doctor_name, doctor_specialization }, doctorRepository)

    // output
    if (_.isArray(result)) res.json({ status: 'failed', message: 'error occured', error: result })
    else {
        const doctorSerializer = new DoctorSerializer()
        res.status(201).json({
            status: 'success',
            message: 'Doctor created successfully',
            doctor: doctorSerializer.serialize(result)
        })
    }
})
//////signup

////update by id
router.put('/', async (req, res) => {
    // input
    const { id, doctor_name, doctor_specialization } = req.body

    // treatment
    const result = await doctorUseCases.updateDoctor(id, { doctor_name, doctor_specialization }, doctorRepository)
    // output
    if (result[0] == 0) res.json({ status: 'failed', message: 'failed to update Doctor', error: 'could not find the doctor with the id given' })
    if (result[0] == 1) res.json({ status: 'success', message: 'doctor updated successfully' })
    res.json({ status: 'Failed', message: 'error occured', error: result })

})
////update by id
router.put('/user_profile',async (req, res) => {
    // input

console.log("coming to user_profiles")

     const { id,user_id, role_id, role_name ,profile} = req.body

    // treatment
    const result = await userprofileUseCases.update(id, { user_id, role_id, role_name ,profile }, userprofileRepository)
    // output
    // if (result[0] == 0) res.json({ status: 'failed', message: 'failed to update Profile', error: 'could not find the member with the id given' })
    // if (result[0] == 1) res.json({ status: 'success', message: 'Profile updated successfully' })
    // res.json({ status: 'Failed', message: 'error occured', error: result })
     if (result.length < 1) res.json({ status: 'success', message: 'There are no Profile in the database' })
    else {
       // const userprofileSerializer = new UserProfileSerializer()
        res.json({
            status: 'success',
            message: 'Updated Successfully',
            //result: userprofileSerializer.serialize(result)
        })
    }

})

router.put('/user_profile/update',upload.single('image'), async (req, res) => {
    // input

console.log("coming to user_profiles")
 const { id, user_id, role_id,email,mobile,image,role_name,qualification,aboutme,gender } = req.body
  // console.log(req.image.path);
     console.log("coming to images")
//const { filename, mimetype, size, path } = req.file;
  //console.log(req.body.image)
  //console.log(req.file.filename)

const filepath = req.file.filename;
console.log(filepath)
    //console.log(req.file);
    // console.log("coming to images")
//const { filename, mimetype, size, path } = req.file;
 console.log("coming to path here")
 const profile2 = {"name":req.body.name,"email":req.body.email,"gender":req.body.gender,"image":filepath,"mobile":req.body.mobile,"qualification":[req.body.qualification],"address":{"state":req.body.state,"city":req.body.city},"languages_known":[],"about_me":req.body.aboutme}
 console.log(profile2)
    // const { id,user_id, role_id, role_name ,profile} = req.body
//
    // treatments
    const result = await userprofileUseCases.update(id, { user_id, role_id, role_name ,profile:profile2 }, userprofileRepository)
    // output
    // if (result[0] == 0) res.json({ status: 'failed', message: 'failed to update Profile', error: 'could not find the member with the id given' })
    // if (result[0] == 1) res.json({ status: 'success', message: 'Profile updated successfully' })
    // res.json({ status: 'Failed', message: 'error occured', error: result })
     if (result.length < 1) res.json({ status: 'success', message: 'There are no Profile in the database' })
    else {
        //const userprofileSerializer = new UserProfileSerializer()
        res.json({
            status: 'success',
            message: 'Updated Successfully',
            //result: userprofileSerializer.serialize(result)
        })
    }

})


/////get all
router.get('/', async (req, res) => {
         console.log("coming 1st")
    const result = await doctorUseCases.getAllDoctors(doctorRepository)
    // output
    if (result.length < 1) res.json({ status: 'success', message: 'There are no doctors in the database' })
    else {
        const doctorSerializer = new DoctorSerializer()
        res.json({
            status: 'success',
            message: 'doctor found',
            result: doctorSerializer.serialize(result)
        })
    }
})
/////get all

////delete by id
router.delete('/', async (req, res) => {
    // input
    const { id } = req.body

    // treatment 
    const result = await doctorUseCases.deleteDoctor(id, doctorRepository)
    // output
    if (_.isString(result)) res.json({ status: 'Failed', message: 'error occured', error: result })
    if (result == 0) res.json({ status: 'Failed', message: 'Could not find the doctor with given Id' })
    else res.json({ status: 'success', message: 'doctor deleted successfully' })
})

////delete by id



/////get by id
router.get('/id', async (req, res) => {
    // input
    console.log("getting doctor by  here")
    const { id } = req.body
 console.log(id)
    // treatment 
    const result = await doctorUseCases.getSingleDoctor(id, doctorRepository)
    console.log(result)
    // output
    if (_.isString(result)) res.json({ status: 'Failed', message: 'error occured', error: result })
    if (result == 0) res.json({ status: 'Failed', message: 'Could not fetch the doctor with given Id' })
    else res.json({ status: 'success', message: result })
})

/////get by id



/////login
router.post('/login', async (req, res) => {
    // inputs
    console.log("coming to login method")
    const { doctor_name, doctor_specialization } = req.body
console.log(req.body)
    
    const result = await doctorUseCases.logincheck({ doctor_name, doctor_specialization }, doctorRepository)

  if (result == 0) res.json({ status: 'Failed', message: 'Invalid credintials' })
    else {
        if (doctor_specialization != result[0].doctor_specialization){
            res.json({ status: 'Failed', message: 'Invalid credintials' })
        }
        else{
            const token = jwt.sign({ doctor_name }, jwtKey, {
		algorithm: "HS256",
		expiresIn: jwtExpirySeconds,
	})
	console.log("token:", token)
        const doctorSerializer = new DoctorSerializer()
        res.status(200).json({
            status: 'success',
            message: 'login successful',
            doctor: {"token":token}
        })
    }
    }
})

/////login

module.exports = router
