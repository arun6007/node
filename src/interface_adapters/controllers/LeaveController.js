const router = require('express').Router()
const _ = require('lodash')
const jwt = require('jsonwebtoken')
const config = require('../../../config')
const message = require('../../../message')
const LeaveSerializer = require('../serializer/LeaveSerializer')

const LeaveUseCases = require('../../application_business_rules/use_cases/LeaveUseCases')
const LeaveRepositoryMyql = require('../storage/LeaveRepositoryMySql')
const LeaveRepository = require('../../application_business_rules/repositories/LeaveRepository')
const jwtKey = "It's a secret"
const jwtExpirySeconds = 300
////main
// initializing city repository 
const leaveRepository = new LeaveRepository(new LeaveRepositoryMyql())
const leaveUseCases = new LeaveUseCases()
//////signup
router.post('/add', async (req, res) => {
    // inputs
    const {  user_id, start_date, end_date, reason, status, approved_id, leave_type } = req.body

    // treatment
    const result = await leaveUseCases.addLeave({  user_id, start_date, end_date, reason, status, approved_id, leave_type }, leaveRepository)

    // output
    if (_.isArray(result)) res.json({ status: 'failed', message: 'error occured', error: result })
    else {
        const leaveSerializer = new LeaveSerializer()
        res.status(201).json({
            status: 'success',
            message: 'Leave created successfully',
            leave: [leaveSerializer.serialize(result)]
        })
    }
})
//////signup

////update by id
router.put('/', async (req, res) => {
    // input
    const { id, username, password ,name, role } = req.body

    // treatment
    const result = await doctorUseCases.updateDoctor(id, { doctor_name, doctor_specialization }, doctorRepository)
    // output
    if (result[0] == 0) res.json({ status: 'failed', message: 'failed to update Doctor', error: 'could not find the doctor with the id given' })
    if (result[0] == 1) res.json({ status: 'success', message: 'doctor updated successfully' })
    res.json({ status: 'Failed', message: 'error occured', error: result })

})
////update by id
router.put('/user', async (req, res) => {
    // input

console.log("coming to users")

     const { id,username, password ,name, role} = req.body

    // treatment
    const result = await userUseCases.update(id, { username, password ,name, role }, userRepository)
    // output
    // if (result[0] == 0) res.json({ status: 'failed', message: 'failed to update Profile', error: 'could not find the member with the id given' })
    // if (result[0] == 1) res.json({ status: 'success', message: 'Profile updated successfully' })
    // res.json({ status: 'Failed', message: 'error occured', error: result })
     if (result.length < 1) res.json({ status: 'success', message: 'There are no Profile in the database' })
    else {
       // const userprofileSerializer = new UserProfileSerializer()
        res.json({
            status: 'success',
            message: 'Updated Successfully',
            //result: userprofileSerializer.serialize(result)
        })
    }

})


/////get all
router.get('/all', async (req, res) => {
         console.log("coming 1st")
          const { user_id } = req.headers
 console.log(user_id)
    const result = await leaveUseCases.getLeavesById(user_id,leaveRepository)
    // output
    if (result.length < 1) res.json({ status: 'success', message: 'There are no leaves in the database' })
    else {
        const leaveSerializer = new LeaveSerializer()
        res.json({
            status: 'success',
            message: 'leaves found',
            result: leaveSerializer.serialize(result)
        })
    }
})
/////get all

////delete by id
router.delete('/', async (req, res) => {
    // input
    const { id } = req.body

    // treatment 
    const result = await doctorUseCases.deleteDoctor(id, doctorRepository)
    // output
    if (_.isString(result)) res.json({ status: 'Failed', message: 'error occured', error: result })
    if (result == 0) res.json({ status: 'Failed', message: 'Could not find the doctor with given Id' })
    else res.json({ status: 'success', message: 'doctor deleted successfully' })
})

////delete by id



/////get by id
router.get('/id', async (req, res) => {
    // input
    console.log("getting doctor by  here")
    const { id } = req.body
 console.log(id)
    // treatment 
    const result = await doctorUseCases.getSingleDoctor(id, doctorRepository)
    console.log(result)
    // output
    if (_.isString(result)) res.json({ status: 'Failed', message: 'error occured', error: result })
    if (result == 0) res.json({ status: 'Failed', message: 'Could not fetch the doctor with given Id' })
    else res.json({ status: 'success', message: result })
})

/////get by id



/////login
router.post('/login', async (req, res) => {
    // inputs
    console.log("coming to login method")
    const { doctor_name, doctor_specialization } = req.body
console.log(req.body)
    
    const result = await doctorUseCases.logincheck({ doctor_name, doctor_specialization }, doctorRepository)

  if (result == 0) res.json({ status: 'Failed', message: 'Invalid credintials' })
    else {
        if (doctor_specialization != result[0].doctor_specialization){
            res.json({ status: 'Failed', message: 'Invalid credintials' })
        }
        else{
            const token = jwt.sign({ doctor_name }, jwtKey, {
		algorithm: "HS256",
		expiresIn: jwtExpirySeconds,
	})
	console.log("token:", token)
        const doctorSerializer = new DoctorSerializer()
        res.status(200).json({
            status: 'success',
            message: 'login successful',
            doctor: {"token":token}
        })
    }
    }
})

/////login

module.exports = router
