const sequelize = require('../../frameworks_drivers/database/sequelize')
const _ = require('lodash')

module.exports = class {
    constructor() {
        this.db = sequelize
        this.model = this.db.model('users')
    }
    async add(doctorEntity) {
        const err = []
        const { doctor_name, doctor_specialization } = doctorEntity
        if (_.isUndefined(doctor_name) || _.isNull(doctor_name)) err.push("doctor name is required in field 'doctor_name'")
        if (_.isUndefined(doctor_specialization) || _.isNull(doctor_specialization)) err.push("doctor_specialization is required in field 'doctor_specialization'")

        if (err.length > 0) return err
        else {
            return await this.model.create({ doctor_name, doctor_specialization }, { raw: true })
        }


    }

    async update(id, userEntity) {
        console.log("coming to storage")
        const err = []
        const {  username, password ,name, role } = userEntity
       // if(_.isUndefined(user_id) || _.isNull(user_id)) err.push("Cannot update user_id if Id is not provided. Add id in field 'id'.")
       // if (_.isUndefined(role_id) || _.isNull(role_id)) err.push("role_id is required in field 'role_id'")
       // if (_.isUndefined(role_name) || _.isNull(role_name)) err.push("role_name is required in field 'role_name'")     
        if(err.length > 0) return err
        return await this.model.update({ username, password ,name, role }, { where: { id }, raw: true })

    }

    async delete(id) {
        if(_.isUndefined(id) || _.isNull(id)) return 'Could not delete city without id'
        return await this.model.destroy({ where: { id } })
            
    }

    async getAll(id) {
        return await this.model.findAll({ where: { id } })
    }
    async get(id) {
        if(_.isUndefined(id) || _.isNull(id)) return 'Could not get details without id'
        return await this.model.findAll({ where: { id } })
       
    }
    async check(doctorEntity) {
        // if(_.isUndefined(doctor_name) || _.isNull(doctor_name)) return 'Could not get details without id'
        // return await this.model.findAll({ where: { doctor_name } })
         const err = []
        const { doctor_name, doctor_specialization } = doctorEntity
        if (_.isUndefined(doctor_name) || _.isNull(doctor_name)) err.push("doctor name is required in field 'doctor_name'")
        if (_.isUndefined(doctor_specialization) || _.isNull(doctor_specialization)) err.push("doctor_specialization is required in field 'doctor_specialization'")

        if (err.length > 0) return err
        else {
            const v1=this.model.findAll({ where: { doctor_name } })
            //if (doctor_name) && doctor_specialization) != this.model.findAll
            // if (doctor_name && doctor_specialization != v1.doctor_name && v1.doctor_specialization){
            //     return 'invalid credintials'
            // }
        return await this.model.findAll({ where: { doctor_name } })
// if this.model.findAll({ where: { doctor_name } }) == true{
//     console.log("going to model")
//            return await this.model.findAll({ where: { doctor_name } })
// }
        }
       
    }
}